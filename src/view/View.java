package view;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import controller.Controller;
import model.data_structures.Nodo;
import model.data_structures.Queue;
import model.data_structures.interfaces.ILista;
import model.logic.Manager;
import model.vo.Bike;
import model.vo.Station;
import model.vo.Trip;

public class View {

	public static void main(String[] args){

		Scanner linea = new Scanner(System.in);
		boolean fin = false; 
		Controller controlador = new Controller();
		while(!fin)
		{
			//Muestra cual fuente de datos va a cargar
			printMenu();

			int option = linea.nextInt();
			switch(option)
			{
			
			case 1:  //Carga de datos 1C
				String dataTrips = "";  // ruta del archivo de Trips
				int tripsIdFile = 0;
				String dataStations = ""; // ruta del archivo de Stations
				int stationsIdFile = 0;
				
				printMenuCargar();
				int tamanoDatos = linea.nextInt();
				switch (tamanoDatos)
				{
				case 1:
					controlador.resetStations();
					dataTrips = Manager.TRIPS_Q1;
					tripsIdFile = 1;
					dataStations = Manager.STATIONS_Q3_Q4;
					stationsIdFile = 2;					
					
					//Memoria y tiempo
					long memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
					long startTime = System.currentTimeMillis();

					//Metodo 1C
					Controller.C1cargar(dataTrips, tripsIdFile , dataStations, stationsIdFile );
					
					System.out.println("\nTotal trips cargados en el sistema: " + controlador.getListaTrips().size() );
					
					System.out.println("Total estaciones cargadas en el sistema: " + controlador.getListaEstaciones().size() + "\n");
					
					//Tiempo en cargar
					long endTime = System.currentTimeMillis();
					long duration = endTime - startTime;

					//Memoria usada
					long memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
					System.out.println("Tiempo en cargar: " + ((double)duration/(1000.00)) + " segundos \nMemoria utilizada:  "+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000.0) + " MB\n\n");
					
					break;
					
				case 2:
					controlador.resetStations();
					dataTrips = Manager.TRIPS_Q2;
					tripsIdFile = 2;
					dataStations = Manager.STATIONS_Q3_Q4;
					stationsIdFile = 2;
					
					//Memoria y tiempo
					long memoryBeforeCase2 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
					long startTime2 = System.currentTimeMillis();

					//Metodo 1C
					Controller.C1cargar(dataTrips, tripsIdFile , dataStations, stationsIdFile );
					
					System.out.println("\nTotal trips cargados en el sistema: " + controlador.getListaTrips().size() );
					
					System.out.println("Total estaciones cargadas en el sistema: " + controlador.getListaEstaciones().size() + "\n");
					
					//Tiempo en cargar
					long endTime2 = System.currentTimeMillis();
					long duration2 = endTime2 - startTime2;

					//Memoria usada
					long memoryAfterCase2 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
					System.out.println("Tiempo en cargar: " + ((double)duration2/(1000.00)) + " segundos \nMemoria utilizada:  "+ ((memoryAfterCase2 - memoryBeforeCase2)/1000000.0) + " MB\n\n");
					
					break;
					
				case 3:
					controlador.resetStations();
					dataTrips = Manager.TRIPS_Q3;
					tripsIdFile = 3;
					dataStations = Manager.STATIONS_Q3_Q4;
					stationsIdFile = 2;
					
					
					//Memoria y tiempo
					long memoryBeforeCase3 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
					long startTime3 = System.currentTimeMillis();

					//Metodo 1C
					Controller.C1cargar(dataTrips, tripsIdFile , dataStations, stationsIdFile );
					
					System.out.println("\nTotal trips cargados en el sistema: " + controlador.getListaTrips().size() );
					
					System.out.println("Total estaciones cargadas en el sistema: " + controlador.getListaEstaciones().size() + "\n");
					
					//Tiempo en cargar
					long endTime3 = System.currentTimeMillis();
					long duration3 = endTime3 - startTime3;

					//Memoria usada
					long memoryAfterCase3 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
					System.out.println("Tiempo en cargar: " + ((double)duration3/(1000.00)) + " segundos \nMemoria utilizada:  "+ ((memoryAfterCase3 - memoryBeforeCase3)/1000000.0) + " MB\n\n");
					
					break;
					
				case 4:
					controlador.resetStations();
					dataTrips = Manager.TRIPS_Q4;
					tripsIdFile = 4;
					dataStations = Manager.STATIONS_Q3_Q4;
					stationsIdFile = 2;
					
					//Memoria y tiempo
					long memoryBeforeCase4 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
					long startTime4 = System.currentTimeMillis();

					//Metodo 1C
					Controller.C1cargar(dataTrips, tripsIdFile , dataStations, stationsIdFile );
					
					System.out.println("\nTotal trips cargados en el sistema: " + controlador.getListaTrips().size() );
					
					System.out.println("Total estaciones cargadas en el sistema: " + controlador.getListaEstaciones().size() + "\n");
					
					//Tiempo en cargar
					long endTime4 = System.currentTimeMillis();
					long duration4 = endTime4 - startTime4;

					//Memoria usada
					long memoryAfterCase4 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
					System.out.println("Tiempo en cargar: " + ((double)duration4/(1000.00)) + " segundos \nMemoria utilizada:  "+ ((memoryAfterCase4 - memoryBeforeCase4)/1000000.0) + " MB\n\n");
					
					break;
				case 5:
					controlador.resetAll();
					
					//Memoria y tiempo
					long memoryBeforeCase5 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
					long startTime5 = System.currentTimeMillis();

					//Metodo 1C
					
					dataStations = Manager.STATIONS_Q3_Q4;
					controlador.C1cargarTrips(Manager.TRIPS_Q4, 4);
					controlador.C1cargarTrips(Manager.TRIPS_Q3, 3);
					controlador.C1cargarTrips(Manager.TRIPS_Q2, 2);
					controlador.C1cargarTrips(Manager.TRIPS_Q1, 1);
					controlador.C1cargarStations(dataStations, 2);				

					
					System.out.println("\nTotal trips cargados en el sistema: " + controlador.getListaTrips().size() );
					
					System.out.println("Total estaciones cargadas en el sistema: " + controlador.getListaEstaciones().size() + "\n");
					
					//Tiempo en cargar
					long endTime5 = System.currentTimeMillis();
					long duration5 = endTime5 - startTime5;

					//Memoria usada
					long memoryAfterCase5 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
					System.out.println("Tiempo en cargar: " + ((double)duration5/(1000.00)) + " segundos \nMemoria utilizada:  "+ ((memoryAfterCase5 - memoryBeforeCase5)/1000000.0) + " MB\n\n");
					
					
					break;
					
				case 6:				
					controlador.loadBikes();
					break;
				
				case 7: // Opcion para reiniciar los datos del sistema. Conjunto vacio de trips y de estaciones.

					controlador.resetAll();
					
					break;
				}
				break;
				
				
			case 2: //Req 1A
				
				//Fecha Inicial
				System.out.println("Ingrese la fecha inicial (Ej : 28/3/2017)");
				String fechaInicialReq1A = linea.next();
				
				//Hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00)");
				String horaInicialReq1A = linea.next();
				
				// Datos Fecha/Hora inicial
				Date localDateInicio = convertirFecha_Hora_LDT(fechaInicialReq1A, horaInicialReq1A);

				//fecha final
				System.out.println("Ingrese la fecha final (Ej : 29/3/2017)");
				String fechaFinalReq1A = linea.next();
				
				//hora final
				System.out.println("Ingrese la hora final (Ej: 14:25:30)");
				String horaFinalReq1A = linea.next();
				
				// Datos Fecha/Hora final
				Date localDateFin = convertirFecha_Hora_LDT(fechaFinalReq1A, horaFinalReq1A);

				//Metodo 1AF
				Queue<Trip> colaDeViajes = Controller.A1(localDateInicio, localDateFin);
				Nodo<Trip> iterador1A = colaDeViajes.getFirstNodo();
				while(iterador1A.getNext() != null)
				{   // Mostrar un viaje en una misma linea
					Trip v = iterador1A.getitem();
					System.out.print("Id trip: " + v.getTripId() + ", ");
					System.out.print("Id bicicleta: "+ v.getBikeId() + ", ");
					System.out.print("Fecha de inicio: "+ v.getStartTime() + ", ");
					System.out.println("Fecha de fin: "+ v.getStopTime());
					System.out.println("-----");
					iterador1A = iterador1A.getNext();
				}
				break;
			
			case 3: //Req 2A
				//Fecha Inicial
				System.out.println("Ingrese la fecha inicial (Ej : 28/3/2017)");
				String fechaInicialReq2A = linea.next();
				
				//Hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00)");
				String horaInicialReq2A = linea.next();
				
				// Datos Fecha/Hora inicial
				Date localDateInicio2A = convertirFecha_Hora_LDT(fechaInicialReq2A, horaInicialReq2A);

				//fecha final
				System.out.println("Ingrese la fecha final (Ej : 29/3/2017)");
				String fechaFinalReq2A = linea.next();
				
				//hora final
				System.out.println("Ingrese la hora final (Ej: 14:25:30)");
				String horaFinalReq2A = linea.next();
				
				// Datos Fecha/Hora final
				Date localDateFin2A = convertirFecha_Hora_LDT(fechaFinalReq2A, horaFinalReq2A);

				//Metodo 2A
				ILista<Bike> bicicletasOrdenadas2A = Controller.A2(localDateInicio2A, localDateFin2A);
				
				Nodo<Bike> iterador2A = bicicletasOrdenadas2A.getFirstNodo();
				
				while(iterador2A.getNext() != null)
				{
					Bike b = iterador2A.getitem();
					System.out.print("Bicicleta Id: " + b.getBikeId() + ", ");
					System.out.print("Total de viajes: "+ b.getTotalTrips() + ", ");
					System.out.println("Total distancia recorrida: "+ b.getTotalDistance());
					System.out.println("-----");
					
					iterador2A = iterador2A.getNext();
				}
				break;
				
			case 4: //Req 3A
				//Id de la bicicleta
				System.out.println("Ingrese el id de la Bicleta: ");
				int idBicicleta3A = Integer.parseInt(linea.next());
				
				
				//Fecha Inicial
				System.out.println("Ingrese la fecha inicial (Ej : 28/3/2017)");
				String fechaInicialReq3A = linea.next();
				
				//Hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00)");
				String horaInicialReq3A = linea.next();
				
				// Datos Fecha/Hora inicial
				Date localDateInicio3A = convertirFecha_Hora_LDT(fechaInicialReq3A, horaInicialReq3A);

				//fecha final
				System.out.println("Ingrese la fecha final (Ej : 29/3/2017)");
				String fechaFinalReq3A = linea.next();
				
				//hora final
				System.out.println("Ingrese la hora final (Ej: 14:25:30)");
				String horaFinalReq3A = linea.next();
				
				// Datos Fecha/Hora final
				Date localDateFin3A = convertirFecha_Hora_LDT(fechaFinalReq3A, horaFinalReq3A);

				//Metodo 3A
				ILista<Trip> viajesPorBicicleta = controlador.A3(idBicicleta3A, localDateInicio3A, localDateFin3A);
				
				Nodo<Trip> iterador3A = viajesPorBicicleta.getFirstNodo();
				
				while(iterador3A.getNext() != null)
				{
					Trip v = iterador3A.getitem();
					System.out.print("Trip id: "+ v.getTripId() + ", ");
					System.out.print("Fecha Inicio: "+ v.getStartTime() + ", ");
					System.out.println("Fecha Fin: "+ v.getStopTime());
					System.out.println("-----");
					iterador3A = iterador3A.getNext();
				}
				break;
				
			case 5: //Req 4A
				//Id estacion final:
				System.out.println("Ingrese id de la estacion final: ");
				int idEstacionFinal4A = Integer.parseInt(linea.next());
				
				//Fecha Inicial
				System.out.println("Ingrese la fecha inicial (Ej : 28/3/2017)");
				String fechaInicialReq4A = linea.next();
				
				//Hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00)");
				String horaInicialReq4A = linea.next();
				
				// Datos Fecha/Hora inicial
				Date localDateInicio4A = convertirFecha_Hora_LDT(fechaInicialReq4A, horaInicialReq4A);

				//fecha final
				System.out.println("Ingrese la fecha final (Ej : 29/3/2017)");
				String fechaFinalReq4A = linea.next();
				
				//hora final
				System.out.println("Ingrese la hora final (Ej: 14:25:30)");
				String horaFinalReq4A = linea.next();
				
				// Datos Fecha/Hora final
				Date localDateFin4A = convertirFecha_Hora_LDT(fechaFinalReq4A, horaFinalReq4A);

				//Metodo 4A
				ILista<Trip> viajesEstacionFinal = controlador.A4(idEstacionFinal4A, localDateInicio4A, localDateFin4A);
				
				Nodo<Trip> iterador4A = viajesEstacionFinal.getFirstNodo();
				
				while(iterador4A.getNext() != null)
				{
					Trip v = iterador4A.getitem();
					System.out.print("Trip ID: "+ v.getTripId() + ", ");
					System.out.print("Bike ID: "+ v.getBikeId() + ", ");
					System.out.println("Fecha terminacion: " + v.getStopTime());
					System.out.println("-----");
					iterador4A = iterador4A.getNext();
				}
				break;
				
			case 6: //Req 1B
				//Fecha Inicial
				System.out.println("Ingrese la fecha inicial (Ej : 28/3/2017)");
				String fechaInicialReq1B = linea.next();
				
				//Hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00)");
				String horaInicialReq1B = linea.next();
				
				// Datos Fecha/Hora inicial
				Date localDateInicio1B = convertirFecha_Hora_LDT(fechaInicialReq1B, horaInicialReq1B);

				//Req 1B
				Queue<Station> estacionesFechaInicio = controlador.B1(localDateInicio1B);
				
				Nodo<Station> iterador1B = estacionesFechaInicio.getFirstNodo();
				
				while(iterador1B.getNext() != null)
				{
					Station s = iterador1B.getitem();
					System.out.print("Estacion id: "+ s.getStationId() + ", ");
					System.out.print("Estacion Nombre: "+ s.getStationName() + ", ");
					System.out.println("Fecha de Inicio Operaciones: "+ s.getStartDate());
					System.out.println("-----");
					iterador1B = iterador1B.getNext();					
				}
				break;
				
			case 7: //Req 2B
				//Fecha Inicial
				System.out.println("Ingrese la fecha inicial (Ej : 28/3/2017)");
				String fechaInicialReq2B = linea.next();
				
				//Hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00)");
				String horaInicialReq2B = linea.next();
				
				// Datos Fecha/Hora inicial
				Date localDateInicio2B = convertirFecha_Hora_LDT(fechaInicialReq2B, horaInicialReq2B);

				//fecha final
				System.out.println("Ingrese la fecha final (Ej : 29/3/2017)");
				String fechaFinalReq2B = linea.next();
				
				//hora final
				System.out.println("Ingrese la hora final (Ej: 14:25:30)");
				String horaFinalReq2B = linea.next();
				
				// Datos Fecha/Hora final
				Date localDateFin2B = convertirFecha_Hora_LDT(fechaFinalReq2B, horaFinalReq2B);

				//Metodo 2B
				ILista<Bike> bicicletasOrdenadasPorDistancia = controlador.B2(localDateInicio2B, localDateFin2B);
				if(bicicletasOrdenadasPorDistancia.isEmpty())
				{
					System.out.println("No se encontraron viajes aqui");
				}else
				{

					System.out.print("Bicicleta Id: "+ bicicletasOrdenadasPorDistancia.getFirst().getBikeId() + ", ");
					System.out.print("Distancia Total: " + bicicletasOrdenadasPorDistancia.getFirst().getTotalDistance() + ", ");
					System.out.println("Viajes Totales: " + bicicletasOrdenadasPorDistancia.getFirst().getTotalTrips());
					System.out.println("-----");
					
					Nodo<Bike> iterador2B = bicicletasOrdenadasPorDistancia.getFirstNodo();
					
					while(iterador2B.getNext() != null)
					{
						Bike b = iterador2B.getitem();
						System.out.print("Bicicleta Id: "+ b.getBikeId() + ", ");
						System.out.print("Distancia Total: " + b.getTotalDistance() + ", ");
						System.out.println("Viajes Totales: " + b.getTotalTrips());
						System.out.println("-----");
						iterador2B = iterador2B.getNext();
					}
					
					System.out.println(bicicletasOrdenadasPorDistancia.size() + " bicicletas en este rango");
				}
				break;
				
			case 8: //Req 3B
				System.out.println("Ingrese bicicleta Id:");
				int bicicletaId3B = Integer.parseInt(linea.next());
				
				System.out.println("Ingrese tiempo maximo");
				int tiempoMaximo3B = Integer.parseInt(linea.next());
				
				System.out.println("Ingrese genero");
				String genero3B = linea.next();
				
				ILista<Trip> viajesporBicicletaDuracion = controlador.B3(bicicletaId3B, tiempoMaximo3B, genero3B);
				
				Nodo<Trip> iterador3B = viajesporBicicletaDuracion.getFirstNodo();
				
				while(iterador3B.getNext() != null)
				{
					Trip t = iterador3B.getitem();
					System.out.print("Trip Id: "+ t.getTripId() + ", ");
					System.out.print("Fecha inicial: "+ t.getStartTime() + ", ");
					System.out.print("Fecha final: "+ t.getStopTime() + ", ");
					System.out.println("Duracion viaje: "+ t.getTripDuration());
					System.out.println("-----");
					iterador3B = iterador3B.getNext();					
				}
				break;
				
			case 9: //Req 4B
				System.out.println("Ingrese identificador estacion: ");
				int estacionInicioId = Integer.parseInt(linea.next());
				
				//Fecha Inicial
				System.out.println("Ingrese la fecha inicial (Ej : 28/3/2017)");
				String fechaInicialReq4B = linea.next();
				
				//Hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00)");
				String horaInicialReq4B = linea.next();
				
				// Datos Fecha/Hora inicial
				Date localDateInicio4B = convertirFecha_Hora_LDT(fechaInicialReq4B, horaInicialReq4B);

				//fecha final
				System.out.println("Ingrese la fecha final (Ej : 29/3/2017)");
				String fechaFinalReq4B = linea.next();
				
				//hora final
				System.out.println("Ingrese la hora final (Ej: 14:25:30)");
				String horaFinalReq4B = linea.next();
				
				// Datos Fecha/Hora final
				Date localDateFin4B = convertirFecha_Hora_LDT(fechaFinalReq4B, horaFinalReq4B);

				ILista<Trip> ViajesporEstacionInicial = controlador.B4(estacionInicioId, localDateInicio4B, localDateFin4B);
				
				Nodo<Trip> iterador4B = ViajesporEstacionInicial.getFirstNodo();
				
				while(iterador4B.getNext() != null)
				{
					Trip t = iterador4B.getitem();
					System.out.print("Trip Id: "+ t.getTripId() + ", ");
					System.out.print("Bike Id: "+t.getBikeId() + ", " + "(" + t.getStartStationId()  +")");
					System.out.println("Fecha Inicio: " + t.getStartTime()  );
					System.out.println("-----");
					iterador4B = iterador4B.getNext();
				}
				
				System.out.println("\n\t\t + total trips: " + ViajesporEstacionInicial.size());
				break;
				
				
			case 10: //Req 2C
				System.out.println("Ingrese identificador bicicleta: ");
				int bicicletaId = Integer.parseInt(linea.next());
				
				//Fecha Inicial
				System.out.println("Ingrese la fecha inicial (Ej : 28/3/2017)");
				String fechaInicialReq2C = linea.next();
				
				//Hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00)");
				String horaInicialReq2C = linea.next();
				
				// Datos Fecha/Hora inicial
				Date localDateInicio2C = convertirFecha_Hora_LDT(fechaInicialReq2C, horaInicialReq2C);

				//fecha final
				System.out.println("Ingrese la fecha final (Ej : 2017-02-01)");
				String fechaFinalReq2C = linea.next();
				
				//hora final
				System.out.println("Ingrese la hora final (Ej: 14:25:30)");
				String horaFinalReq2C = linea.next();
				
				// Datos Fecha/Hora final
				Date localDateFin2C = convertirFecha_Hora_LDT(fechaFinalReq2C, horaFinalReq2C);

				// Metodo
				Queue<Trip> viajesValidados2C = controlador.C2ViajesValidadosBicicleta(bicicletaId, localDateInicio2C, localDateFin2C);
				
				
				while(!viajesValidados2C.isEmpty())
				{
					Trip valido = viajesValidados2C.dequeue();
					System.out.print("Tiempo de terminacion: " + valido.getStopTime() + ", ");
					System.out.print("Estacion de terminacion: " + valido.getEndStationId() + ", ");
					Trip invalido = viajesValidados2C.dequeue();
					System.out.print("Tiempo de inicio: "+ invalido.getStartTime() + ", ");
					System.out.println("Estacion de inicio: "+ invalido.getStartStationId());
					System.out.println("-----");
				}
				break;
				
			case 11: //Req 3C
				System.out.println("Ingrese numero de bicicletas: ");
				int numeroBicicletas3C = Integer.parseInt(linea.next());
				
				ILista<Bike> topBicicletas = controlador.C3BicicletasMasUsadas(numeroBicicletas3C);
				int i = 1;
				
				System.out.print((i++) + ". Bike Id: "+ topBicicletas.getFirst().getBikeId() + ", ");
				System.out.println("Duracion total de todos viaje: " + topBicicletas.getFirst().getTotalDuration() + " minutos ~ " + (((double)topBicicletas.getFirst().getTotalDuration())/60) + " horas" );
				
				Nodo<Bike> iterador3C = topBicicletas.getFirstNodo();
				
				while(iterador3C.getNext() != null)
				{
					Bike b = iterador3C.getitem();
					System.out.print((i++) + ". Bike Id: "+ b.getBikeId() + ", ");
					System.out.println("Duracion total de todos viaje: " + b.getTotalDuration() + " minutos ~ " + (((double)b.getTotalDuration())/60) + " horas" );
					iterador3C = iterador3C.getNext();
					
				}
				
				break;
				
			case 12: //Req 4C
				System.out.println("Ingrese Id de estacion: ");
				int idEstacion4C = Integer.parseInt(linea.next());
				
				//Fecha Inicial
				System.out.println("Ingrese la fecha inicial (Ej : 28/3/2017)");
				String fechaInicialReq4C = linea.next();
				
				//Hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00)");
				String horaInicialReq4C = linea.next();
				
				// Datos Fecha/Hora inicial
				Date localDateInicio4C = convertirFecha_Hora_LDT(fechaInicialReq4C, horaInicialReq4C);

				//fecha final
				System.out.println("Ingrese la fecha final (Ej : 21/2/2017)");
				String fechaFinalReq4C = linea.next();
				
				//hora final
				System.out.println("Ingrese la hora final (Ej: 14:25:30)");
				String horaFinalReq4C = linea.next();
				
				// Datos Fecha/Hora final
				Date localDateFin4C = convertirFecha_Hora_LDT(fechaFinalReq4C, horaFinalReq4C);

				// Metodo
				ILista<Trip> viajesDeEstacion4C = controlador.C4ViajesEstacion(idEstacion4C, localDateInicio4C, localDateFin4C);
				System.out.println("\n\n");
				
				if(viajesDeEstacion4C.isEmpty())
				{
					System.out.println("No se encontaron viajes de la es tacion " + idEstacion4C + " en este rango");
				}else
				{
					Nodo<Trip> iterador4C = viajesDeEstacion4C.getFirstNodo();
					
					while( iterador4C.getNext() != null)
					{
						Trip t = iterador4C.getitem();
						System.out.print("Trip Id: "+ t.getTripId() + ", ");
						System.out.print("Bike Id: "+ t.getBikeId() + ", ");
						System.out.print("-----");					
						System.out.println(" "+ t.getStartTime() + "  to  "  + t.getStopTime());
						iterador4C = iterador4C.getNext();
					}
				}
				
				break;
				
			case 13: //Salir
				fin = true;
				linea.close();
				break;
			}
		}
	}

	private static void printMenu()
	{
		System.out.println("---------ISIS 1206 - Estructuras de Datos----------");
		System.out.println("-------------------- Proyecto 1   - 2018-2 ----------------------");
		System.out.println("Iniciar la Fuente de Datos a Consultar :");
		System.out.println("1. Actualizar la informacion del sistema con una fuente de datos (2017-Q1, 2017-Q2, 2017-Q3, 2017-Q4)");

		System.out.println("\nParte A:\n");
		System.out.println("2. Obtener la cola con todos los viajes en rango de fecha (1A)");
		System.out.println("3. Obtener las bicicletas ordenadas de mayor a menor por el numero de viajes realizados (2A)");
		System.out.println("4. Obtener los viajes de una bicicleta en un rango de fecha dado (3A)");
		System.out.println("5. Obtener viajes que terminaron en una estacion (4A)");
		
		System.out.println("\nParte B:\n");
		System.out.println("6. Obtener Cola con las estaciones que comenzaron despues de una fecha (1B)");
		System.out.println("7. Bicicletas ordenadas por distancia total recorrida en un rango de fecha (2B)");
		System.out.println("8. Viajes de una bicicleta con duracion menor a una dada (3B)");
		System.out.println("9. Viajes que iniciaron en una estacion en un rango dado. (4B)");

		
		System.out.println("\nParte C:\n");
		System.out.println("10. Validar viajes de una bicicleta (2C)");
		System.out.println("11. Bicicletas mas usadas segun duracion de viaje (3C)");
		System.out.println("12. Viajes que iniciaron en una estacion. (4C)");
		System.out.println("13. Salir");
		System.out.println("Ingrese el numero de la opcion seleccionada y presione <Enter> para confirmar: (e.g., 1):");

	}

	private static void printMenuCargar()
	{
		System.out.println("-- Que fuente de datos desea agregar a los datos del sistema (carga incremental)?");
		System.out.println("-- 1. 2017-Q1");
		System.out.println("-- 2. 2017-Q2");
		System.out.println("-- 3. 2017-Q3");
		System.out.println("-- 4. 2017-Q4");
		System.out.println("-- 5. 2017-Q1-Q4 (TODOS LOS DATOS)");
		System.out.println("-- 6. generar datos de bicicletas con la informacion actual");
		System.out.println("-- 7. Reiniciar datos del sistema");		
		System.out.println("-- Ingrese el numero de la fuente a cargar y presione <Enter> para confirmar: (e.g., 1)");
	}
	
	/**
	 * Convertir fecha y hora a un objeto Date
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @param hora hora en formato hh:mm:ss con hh para hora, mm para minutos y ss para segundos
	 * @return objeto Date con fecha y hora integrados
	 * fecha: dia/mes/a�o
	 * hora: hora:minuto:segundo
	 */
	private static Date convertirFecha_Hora_LDT(String fecha, String hora)
	{
		fecha.trim();
		hora.trim();
		String str = fecha + " " + hora;
		SimpleDateFormat formatter;
		Date date = null;
		
		formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		
		try {
			date = formatter.parse(str);
		} catch (ParseException e) {
			System.out.println("Error leyendo las fechas");
			e.printStackTrace();
		}
	
		return date;
	
	}

}
