package api;

import model.data_structures.*;
import model.data_structures.interfaces.*;
import model.vo.Bike;
import model.vo.Station;
import model.vo.Trip;
import java.util.Date;

public interface IManager {
    
	
	/**
     * Generar una cola con todos los viajes en un periodo de tiempo dado ordenados en orden cronologico por su fecha
     * inicial.
     * @param fechaInicial Fecha inicial del periodo de consulta
     * @param fechaFinal Fecha final del periodo de consulta
     * @return Cola con los viajes ordenados
     */
    Queue<Trip> A1ViajesEnPeriodoDeTiempo(Date fechaInicial, Date fechaFinal);

    /**
     * Mostrar las bicicletas ordenadas de mayor a menor por el numero de viajes realizados en el periodo de consulta
     * @param fechaInicial Fecha inicial del periodo de consulta
     * @param fechaFinal Fecha final del periodo de consulta
     * @return Lista con las bicicletas ordenadas
     */
    ILista<Bike> A2BicicletasOrdenadasPorNumeroViajes(Date fechaInicial, Date fechaFinal);

    /**
     * Mostrar todos los viajes (en orden cronologico) realizados por la bicicleta con el identificador dado en el
     * periodo de tiempo establecido
     * @param bikeId El identificador de la bicicleta de la bicicleta
     * @param fechaInicial Fecha inicial del periodo de consulta
     * @param fechaFinal Fecha final del periodo de consulta
     * @return Lista con los viajes ordenados
     */
    ILista<Trip> A3ViajesPorBicicletaPeriodoTiempo(int bikeId, Date fechaInicial, Date fechaFinal);

    /**
     * Mostrar los viajes (ordenados cronologicamente)que terminaron en la estacion con el identificador dado en el
     * periodo de tiempo de consulta
     * @param endStationId El identificador de la estacion final
     * @param fechaInicial Fecha inicial del periodo de consulta
     * @param fechaFinal Fecha final del periodo de consulta
     * @return Lista con los viajes ordenados
     */
    ILista<Trip> A4ViajesPorEstacionFinal(int endStationId, Date fechaInicial, Date fechaFinal);

    /**
     * Generar una cola con las estaciones que comenzaron su operacion despues de la fecha dada. 
     * @param fechaComienzo Fecha en la que las estaciones comenzaron su operacion
     * @return Cola con las estaciones que cumplen el requisito
     */
    Queue<Station> B1EstacionesPorFechaInicioOperacion(Date fechaComienzo);

    /**
     * Mostrar las bicicletas ordenadas de mayor a menor por la distancia total de sus viajes en el periodo de consulta
     * @param fechaInicial Fecha inicial del periodo de consulta
     * @param fechaFinal Fecha final del periodo de consulta
     * @return Lista con las bicicletas ordenadas
     */
    ILista<Bike> B2BicicletasOrdenadasPorDistancia(Date fechaInicial, Date fechaFinal);

    /**
     * Mostrar los viajes (ordenados cronologicamente)realizados por una bicicleta con el identificador dado que tengan
     * una duracion menor al valor de tiempo maximo dado y que hayan sido realizados por una persona del genero dado
     * @param bikeId El identificador de la bicicleta
     * @param tiempoMaximo La duracion maxima de los viajes
     * @param genero El genero de la persona que realizo los viajes
     * @return Lista con los viajes ordenados
     */
    ILista<Trip> B3ViajesPorBicicletaDuracion(int bikeId, int tiempoMaximo, String genero);

    /**
     * Mostrar los viajes (ordenados cronologicamente)que iniciaron en la estacion con el identificador dado en el
     * periodo de tiempo de consulta
     * @param startStationId El identificador de la estacion inicial
     * @param fechaInicial Fecha inicial del periodo de consulta
     * @param fechaFinal Fecha final del periodo de consulta
     * @return Lista con los viajes ordenados
     */
    ILista<Trip> B4ViajesPorEstacionInicial(int startStationId, Date fechaInicial, Date fechaFinal);
    
	/**
	 * Actualizar la informacion del sistema con los datos seleccionados por el usuario y generar/actualizar las estructuras de datos necesarias.
	 * Caso Especial: si rutaTrips y rutaStations son la cadena vacia (""), los datos del sistema deben reiniciarse con un conjunto de trips y de estaciones vacios.
	 * @param rutaTrips ruta del archivo de trips que se va a utilizar
	 * @param 
	 * @param rutaStations ruta del archivo de stations que se va a utilizar
	 */
	void C1cargar(String rutaTrips, int tripsIdFile, String rutaStations, int stationsIdFile);

	
	
	
	/**
	 * actualiza la informacion de las lista de Trips
	 * @param rutaTrips ruta relativa desde la raiz del proyecto
	 * @param tripsIdFile identificador del archivo
	 */
	void C1cargarTrips(String rutaTrips, int tripsIdFile);
	
	
	
	
	/**
	 * actualiza la informacion de las lista de Estaciones
	 * @param rutaTrips ruta relativa desde la raiz del proyecto
	 * @param tripsIdFile identificador del archivo
	 */
	void C1cargarStations(String rutaEstaciones, int estacionesIdFile);
	
	
	
    /**
     * Revisar si todos los viajes de una bicicleta son validos, de tal forma que si un viaje termina en una estacion el siguiente debe iniciar en la misma estacion. 
     * @param bikeId el Id de la bicicleta
     * @param fechaInicial fecha inicial de consulta
     * @param fechaFinal fecha final de consulta
     * @return debe retornar una cola de inconstencias que incluya tiempo de terminacion y estacion de terminacion del viaje validado y tiempo de inicio y estacion de inicio del viaje inconsistente
     * Si no hay viajes inconsistentes debe retornar que todos los viajes son consistentes.
     */
	Queue<Trip> C2ViajesValidadosBicicleta(int bikeId, Date fechaInicial, Date fechaFinal);
    
    /**
     * Retorna las topBicicletas mas utilizadas de acuerdo con la duracion total de viajes.
     * @param topBicicletas el numero de bicicletas que se quieren evaluar.
     * @return Lista de bicicletas que mas se usaron.
     */
    ILista<Bike> C3BicicletasMasUsadas(int topBicicletas);
    
    /**
     * Viajes que iniciaron y terminaron en una estacion dada por su id en una fecha de inicio y fin dada
     * @param StationId id de la estacion
     * @param fechaInicial fecha de inicio de consulta
     * @param fechaFinal fecha de fin de la consulta
     * @return Lista de viajes que iniciaron o terminaron en una estacion en un rango de fechas
     */
    ILista<Trip> C4ViajesEstacion(int StationId, Date fechaInicial, Date fechaFinal);
}
