package controller;

import model.data_structures.*;
import model.data_structures.interfaces.*;
import model.logic.Manager;
import model.vo.Bike;
import model.vo.Station;
import model.vo.Trip;
import java.util.Date;



/**
 * concexion entre la logica y el view
 * @author Andres Forero, Bryan Cuta
 *
 */
public class Controller {
    private static Manager manager = new Manager();

    public static Queue<Trip> A1(Date fechaInicial, Date fechaFinal){
        return manager.A1ViajesEnPeriodoDeTiempo(fechaInicial, fechaFinal);
    }
    

    public static ILista<Bike> A2(Date fechaInicial, Date fechaFinal){
        return manager.A2BicicletasOrdenadasPorNumeroViajes(fechaInicial, fechaFinal);
    }
    

    public ILista<Trip> A3(int bikeId, Date fechaInicial, Date fechaFinal) {
        return manager.A3ViajesPorBicicletaPeriodoTiempo(bikeId, fechaInicial, fechaFinal);
    }
    

    public ILista<Trip> A4(int endStationId, Date fechaInicial, Date fechaFinal) {
        return manager.A4ViajesPorEstacionFinal(endStationId, fechaInicial, fechaFinal);
    }
    

    public Queue<Station> B1(Date fechaComienzo) {
        return manager.B1EstacionesPorFechaInicioOperacion(fechaComienzo);
    }
    

    public ILista<Bike> B2(Date fechaInicial, Date fechaFinal) {
        return manager.B2BicicletasOrdenadasPorDistancia(fechaInicial, fechaFinal);
    }
    

    public ILista<Trip> B3(int bikeId, int tiempoMaximo, String genero) {
        return manager.B3ViajesPorBicicletaDuracion(bikeId, tiempoMaximo, genero);
    }
    

    public ILista<Trip> B4(int startStationId, Date fechaInicial, Date fechaFinal) {
        return manager.B4ViajesPorEstacionInicial(startStationId, fechaInicial, fechaFinal);
    }
    
    
    public static void C1cargar(String dataTrips, int tripsIdFile, String dataStations, int stationsIdFile){
    	manager.C1cargar(dataTrips, tripsIdFile, dataStations, stationsIdFile);
    }
    
    
    
    public void C1cargarTrips(String rutaTrips, int tripsIdFile){
    	manager.C1cargarTrips(rutaTrips, tripsIdFile);
    }         
    
    
    public void C1cargarStations(String rutaEstaciones, int estacionesIdFile){
    	manager.C1cargarStations(rutaEstaciones, estacionesIdFile);
    }   
    
    
        
    public Queue<Trip> C2ViajesValidadosBicicleta(int bikeId, Date fechaInicial, Date fechaFinal){
    	return manager.C2ViajesValidadosBicicleta(bikeId, fechaInicial, fechaFinal);
    }
    
    
    public  ILista<Bike> C3BicicletasMasUsadas(int topBicicletas){
		return manager.C3BicicletasMasUsadas(topBicicletas);
    }
    
    
    public ILista<Trip> C4ViajesEstacion(int StationId, Date fechaInicial, Date fechaFinal){
    	
    	return manager.C4ViajesEstacion(StationId, fechaInicial, fechaFinal);
    	
    }
    
    public void loadBikes()
    {
    	manager.loadBikes();
    }
    
    public ILista<Trip> getListaTrips()
    {
    	return manager.listaTrips;
    }
    
    public ILista<Station> getListaEstaciones()
    {
    	return manager.listaEstaciones;
    }
    
    public void  resetTrips()
    {
    	manager.resetTrips();
    }
    
    public void resetStations()
    {
    	manager.resetStations();
    }
    
    public void resetAll()
    {
    	manager.reset();
    }
    
    
}
