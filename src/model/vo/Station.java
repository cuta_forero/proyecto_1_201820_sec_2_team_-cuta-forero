package model.vo;

import java.util.Date;

import model.data_structures.Lista;


/**
 * clase que representa una estasion
 * 
 * @author Andres Forero, Bryan Cuta
 *
 */
public class Station implements VO<Station> {
	
	/**
	 *  identificador unico de una bicicleta
	 *  factor de orden total(clave 1)
	 */	
	private Integer stationId;
	
	
	/**
	 * 	nombre de una estacion
	 * 	factor de orden total(clave 2)
	 */
	private String stationName;
	
	
	/**
	 * coordenada geografica de latitud 
	 */
	private Double latitude;
	
	
	/**
	 * coordenada geografica de longitud
	 */
	private Double longitude;
	
	
	/**
	 * capacidad documental de una estacion
	 */
	private Integer dpCapacity;	
	
	
	/**
	 * fecha Local de imicio de operacion de la estacion
	 */
	private Date startDate;
	
	
	
	private Lista<Trip> tripsAsociados;
	

	
	public Station(int stationId, String stationName, Double latitude, Double Longitude , int dpCapacity, Date startDate) {
		this.stationId = stationId;
		this.stationName = stationName;
		this.latitude = latitude;
		this.longitude= Longitude;
		this.dpCapacity = dpCapacity;
		this.startDate = startDate;
	}
	
	
	
	

	public int getStationId() {
		return stationId;
	}




	public String getStationName() {
		return stationName;
	}




	public Double getLatitude() {
		return latitude;
	}




	public Double getLongitude() {
		return longitude;
	}




	public int getDpCapacity() {
		return dpCapacity;
	}




	public Date getStartDate() {
		return startDate;
	}
	
	
	
	public void addTripAsociado(Trip p)
	{
		tripsAsociados.addEnd(p);
	}



	
	
	
	
	public String toString()
	{		
		return "" + stationId + ": " + "station# " + stationId + ": " + stationName + " :(" + startDate.toString() + "):";		
	}

	
	
	/**
	 * unique id comparator
	 */
	public int compareTo(Station o) {		
		return stationId.compareTo(o.stationId);
	}
	
	public int compareTo(Station o, int p) {		
		
		int r = 0;		
		
		// compare by id
		if(p == 1)
			r = compareTo(o);
		
		if(p == 2)
			r = stationName.compareTo(o.stationName);
		
		if(p== 3)
			r = startDate.compareTo(o.startDate);
		
		return r;
	}
	
	
	
	
	
}
