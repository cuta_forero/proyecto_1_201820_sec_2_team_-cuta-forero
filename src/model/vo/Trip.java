package model.vo;

import java.util.Date;


/**
 * 
 * Clase que representa un Viaje
 * 
 * @author Andres Forero, Bryan Cuta
 *
 */
public class Trip implements VO<Trip> {

    public final static String MALE = "male";
    public final static String FEMALE = "female";
    public final static String UNKNOWN = "unknown";

    private Integer tripId;
    
    private Date startTime;
    
    private Date stopTime;
    
    private Integer bikeId;
    
    private Integer tripDuration;
    
    private String startStationName;
    
    private Integer startStationId;
    
    private String endStationName;
    
    private Integer endStationId;
    
    private String userType;
    
    private String gender;
    
    private String birthYear;

    public Trip(int tripId, Date startTime, Date stopTime, Integer bikeId, Integer tripDuration, Integer startStationId ,String startStationName, Integer endStationId,  String endStationName, String userType, String gender, String birthYear) {
        
    	this.tripId = tripId;
        this.startTime = startTime;
        this.stopTime = stopTime;
        this.bikeId = bikeId;
        this.tripDuration = tripDuration;
        this.startStationName = startStationName;
        this.startStationId = startStationId;
        this.endStationName = endStationName;
        this.endStationId = endStationId;
        this.userType = userType;
        this.gender = gender;
        this.birthYear = birthYear;
    }

   
  
    



	public Integer getTripId() {
		return tripId;
	}



	public Date getStartTime() {
		return startTime;
	}



	public Date getStopTime() {
		return stopTime;
	}



	public Integer getBikeId() {
		return bikeId;
	}



	public Integer getTripDuration() {
		return tripDuration;
	}



	public String getStartStationName() {
		return startStationName;
	}



	public Integer getStartStationId() {
		return startStationId;
	}



	public String getEndStationName() {
		return endStationName;
	}



	public Integer getEndStationId() {
		return endStationId;
	}



	public String getUserType() {
		return userType;
	}



	public String getGender() {
		return gender;
	}



	public String getBirthYear() {
		return birthYear;
	}


	public String toString(){
    	
    	return "" + tripId + ": " + bikeId + "\t| " + startStationId + " --to--> " + endStationId + "    (" + startTime.toString() + ")" ;
    }

	
	
	/**
	 * unique id comparator
	 */
	public int compareTo(Trip o) {		
		return tripId.compareTo(o.tripId);
	}

	
	
	public int compareTo(Trip o, int p) {
		
		int r = 0;		
		
		// compare by id
		if(p == 1)
			r = compareTo(o);
		
		if(p == 2)
			r = startTime.compareTo(o.startTime);
		
		if(p == 3)
			r = stopTime.compareTo(o.stopTime);
		
		if(p == 4)
			r = bikeId.compareTo(o.bikeId);
		
		if(p == 5)
			r = tripDuration.compareTo(o.tripDuration);
		
		if(p == 6)
			r = startStationId.compareTo(o.startStationId);
		
		if(p == 7)
			r = endStationId.compareTo(o.endStationId);
		
		if(p == 8)
			r = gender.compareTo(o.gender);
		
		if(p == 9)
			r = Integer.valueOf(birthYear).compareTo(Integer.valueOf(o.birthYear));
			
			
		return r;
	}
	
	
    
    



	


	
	
	
	



	
    




	}
