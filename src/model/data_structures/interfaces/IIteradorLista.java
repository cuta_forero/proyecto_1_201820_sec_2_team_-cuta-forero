package model.data_structures.interfaces;

import java.util.Iterator;

public interface IIteradorLista<VO> extends Iterator<VO>{
	
	public VO getCurrent();
	
	public VO getNext();
	
}
