package model.data_structures.interfaces;

import model.data_structures.Nodo;
import model.vo.VO;

/**
 * calse que define las funcionalidades genericas que toda lista dinamica(con puntero en el final) deberia tener
 * super clase de las estructuras: ListaSimple, ListaDoble, Cola, Pila, y algunas listas circulares
 * 
 * @author Andres Forero y Bryan Cuta
 *
 * @param <E> Objeto generico
 */
public interface ILista<E extends VO<E>> extends Iterable<E>{


	/**
	 * retorna un booleano que dice si la lista esta vacia o no
	 */
	public boolean isEmpty();
	
	
	
	/**
	 * retorna el tama�o actual de la lista
	 */
	public int size();
	
	
	
	/**
	 * retorna el primer elemento actual de la lista
	 * @return E primero.item
	 */
	public E getFirst();
	
	
	
	/**
	 * retorna el ultimo elemento actual de la lista
	 * @return E ultimo.item
	 */
	public E getLast();
	
	
	
	/**
	 * agrega un elemento, E nuevo pasado por parametro, al inicio de la lista 
	 */
	public void addFirst(E item);
	
	
	
	/**
	 * agrega un elemento, E nuevo pasado por parametro, al final de la lista 
	 */
	public void addEnd(E item);
	
	
	/**
	 * retorna el primer nodo de la lista
	 */
	public Nodo<E> getFirstNodo();
	
	
	
	/**
	 * retorna el ultimo nodo de la lista
	 */
	public Nodo<E> getLastNodo();
	
	
	/**
	 * eliina el primer elemento de la lista
	 */
	public E removeFirst();
	
	
	/**
	 * elimina el ultimo elemento de la lista
	 */
	public E removeEnd();
	
	
	
	
	/**
	 * retorna el elemento numero k, donde k es un entero pasado por paramentro
	 * @param k: int
	 * @return E item #k
	 */
	public E get(int k);
	
	
	
	/**
	 * 
	 */
	public boolean contains(E item);

	
	/**
	 * vacia la lista
	 */
	public void deleteList();
	
	
	
	

	/**
	 * imprime por consla todos los elementos de la lista
	 */
	public void print();
	
	
	
}
