package model.data_structures;

import java.util.NoSuchElementException;
import model.data_structures.interfaces.IIteradorLista;
import model.data_structures.interfaces.ILista;
import model.vo.VO;



/**
 * Estrctura de Datos Lista simple encadenada con puntero al comienzo al final de la lista
 * 
 * @author Andres Forero, Bryan Cuta
 *
 * @param <E> tipo generico de dato
 */
public class Lista<E extends VO<E>> implements ILista<E>{

	
	
	private Nodo<E> primero;	
	private Nodo<E> ultimo;	
	int size;
	
		
	public Lista()
	{
		primero = ultimo = null;
		size = 0;
	}
	
	public boolean isEmpty() {		
		return primero == null;
	}

	
	
	
	
	public int size() {		
		return size;
	}
	
	
	
	
	
	public void addFirst(E item)
	{
		if(isEmpty())
		{
			primero = ultimo = new Nodo<E>(item);
		}		
		else
		{
			primero = new Nodo<E>(item, primero);
		}		
		size++;
	}
	
	
	
	
	
	public void addEnd(E item)
	{
		if(isEmpty())
			primero = ultimo = new Nodo<E>(item);
		else
			ultimo = ultimo.next = new Nodo<E>(item);
		
		size++;
	}
	
	
	
	
	public E removeFirst()
	{
		if(isEmpty())
			throw new NoSuchElementException();		
		E antiguoPrimero = primero.item;
		
		if(primero == ultimo){
			primero = ultimo = null;
		}
		else{
			primero = primero.next;
		}
		size--;
		return antiguoPrimero;		
	}
	
	
	
	
	public E removeEnd()
	{
		if(isEmpty())
			throw new NoSuchElementException();
		
		E antiguoUltimo = ultimo.item;
		
		if(primero == ultimo)
			primero = ultimo = null;
		else{
			
			Nodo<E> actual = primero;
			
			while(actual.next != ultimo)
			{
				actual = actual.next;
			}
			
			ultimo = actual;
			actual.next = null;				
		}		
		size--;
		
		return antiguoUltimo;		
	}
	
	
	
	
	public E getFirst() {
		return primero.item;
	}


	
	public E getLast() {
		return ultimo.item;
	}

	
	public void deleteList() {
		primero = ultimo = null;
		size = 0;
		
	}
	
	
	public E get(int k) {
		
		E item = null;
		if(k <= size)
		{
			int contador = 1;
			IteradorLista<E> iterador = iterator();
			E actual = iterador.getCurrent();
			while(contador < k)
			{
				actual = iterador.next();
				contador++;
			}
			item = actual;
		}
		return item;
	}
	
	
	
	
	public boolean contains(E item) {
		boolean resp = false;
		if(!isEmpty())
		{
			IIteradorLista<E> iterador = iterator();
			E actual = iterador.getCurrent();
			while(iterador.hasNext())
			{
				if(actual.compareTo(item)==0)
				{
					resp = true;
				}
			}
		}
		return resp;
	}
	
	
	
	public Nodo<E> getFirstNodo()
	{
		return primero;
	}
	
	public Nodo<E> getLastNodo()
	{
		return ultimo;
	}

	

		
	
	public void print()
	{
		Nodo<E> current = primero;
		while(current.next != null)
		{
			System.out.println(current.item.toString());
			current = current.next;
		}
		System.out.println(current.item.toString());
	}
	
	
	
	
		
	
	
	
	
	
	public IteradorLista<E> iterator() {
		
		return new IteradorLista<E>(primero);
	}

	
	public class IteradorLista<T> implements IIteradorLista<T>
	{
		Nodo<T> current;
		
		
		
		
		public IteradorLista(Nodo<T> first)
		{
			current = first;			
		}
		
		
		
		public T getCurrent()
		{
			return current.item;
		}
		
		
		
		
		public boolean hasNext() {
			
			return current.next != null;
		}

		
		public T next() {
			
			if(!hasNext()) throw new NoSuchElementException();
			
			T item = current.next.item;
			current = current.next;			
			return item;
		}
		
		public T getNext()
		{
			return current.next.item;
		}
		
		
		
		
	}
	
	
	
	
	
	
	


	
	
}
