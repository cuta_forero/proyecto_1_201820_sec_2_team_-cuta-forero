package model.data_structures;


/**
 * Nodo simple de una lista dinamica
 * 
 * @author Andres Forero, Bryan Cuta
 *
 * @param <E> tipo de objeto generico
 */
public class Nodo<E> {

	E item;
	
	Nodo<E> next;
	
	
	public Nodo(E objeto)
	{
		this(objeto,null);
	}
	
	
	public Nodo(E objeto, Nodo<E> pNext)
	{
		item = objeto;
		next = pNext;
	}
	
	public Nodo<E> getNext()
	{
		return next;
	}
	
	public E getitem()
	{
		return item;
	}
	
}
